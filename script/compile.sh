#!/bin/bash
clear

g++ -Wno-int-to-pointer-cast src/pcie-tester.cpp -o pcie-tester
g++ src/par-io.cpp -o par-io
g++ -Wno-int-to-pointer-cast src/par-mem.cpp -o par-mem
