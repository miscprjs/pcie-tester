#!/bin/bash

if [[ $EUID -ne 0 ]];
then
	sudo $0
	exit 0
fi

echo -n "0000:01:00.0" > /sys/bus/pci/drivers/serial/unbind
echo -n "0000:01:00.2" > /sys/bus/pci/drivers/parport_serial/unbind
