#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>	
#include <unistd.h>
#include <sys/mman.h>


static uint32_t* memory;


void about() {
	fprintf(stderr,"par-mem: ");
} // about()


void procArgs(int argc, char* argv[]) {
	
	if (argc != 2) {
		about();
		fprintf(stderr, "par-mem <address> \n");
		exit(0);
	}
	
	uint32_t addressInt = strtol(argv[1], NULL, 0);
	if (addressInt == 0) {
		about();
		fprintf(stderr,"invalid address: %s \n", argv[1]);
		exit(1);
	}

	int fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (fd == -1) {
		about();
		fprintf(stderr,"can't open mem file: ");
		perror("");
		exit(1);
	}
	
	void* addressPtr = (void*)addressInt;
	
	memory = (uint32_t*)mmap(
		addressPtr, 
		4096, 
		PROT_READ | PROT_WRITE,
		MAP_SHARED,
		fd,
		0
	);
	
	if (memory == MAP_FAILED) {
		about();
		fprintf(stderr,"can't map memory - ");
		perror("");
		exit(2);
	}
	
} // procArgs()


void writeMem(uint32_t value) {
	
	for (int i = 0; i < 256; i++) {
		memory[i] = value;
	}

} // writeMem()


int main(int argc, char* argv[]) {
	
	procArgs(argc, argv);
	
	printf("%x \n",memory);

	writeMem(~0);
	usleep(500 * 1000);
	writeMem(0);
	
	return 0;
} // main()
