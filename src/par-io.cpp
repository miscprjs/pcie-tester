#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/io.h>


static int port;


void about() {
	fprintf(stderr,"par-io: ");
} // about()


void pwm(int width) {

	outb(0xff, port);
	usleep(width);

	outb(0x00, port);
	usleep(1000 - width);
	
} // pwm()


void procArgs(int argc, char* argv[]) {
	
	if (argc != 2) {
		about();
		fprintf(stderr, "par-io <port> \n");
		exit(0);
	}
	
	port = strtol(argv[1], NULL, 0);
	if (port == 0) {
		about();
		fprintf(stderr,"invalid port: %s \n", argv[1]);
		exit(1);
	}
	
	if (ioperm(port, 1, 1) == -1) {
		about();
		fprintf(stderr,"can't access I/O port at $%x: ", port);
		perror("");
		exit(1);
	}

} // procArgs()


int main(int argc, char* argv[]) {
	
	procArgs(argc, argv);

	for (int i = 0; i < 999; i += 4) {
		pwm(i);
	}

	outb(0xff, port);
	usleep(400 * 1000);
	
	for (int i = 999; i > 0; i -= 5) {
		pwm(i);
	}
	
	return 0;
} // main()
