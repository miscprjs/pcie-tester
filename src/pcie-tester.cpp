#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>	
#include <unistd.h>
#include <sys/mman.h>


static uint32_t* memory;
static size_t dumpOffset;
static int setOffset;
static uint32_t setValue;

static const int size = 32;

enum {
	P_ADDRESS = 1,
	P_DUMP_OFFSET,
	P_SET_OFFSET,
	P_SET_VALUE,
	P_ALL_ARGS_SET
};

void about() {
	fprintf(stderr,"pcie-tester: ");
} // about()


void procArgs(int argc, char* argv[]) {
	
	if ((argc != 3) && (argc != 5)) {
		about();
		fprintf(
			stderr,
			"pcie-tester <address> <dump_offset> [<set_offset> <set_value_32>] \n"
		);
		exit(0);
	}
	
	setOffset = -1;
	if (argc == P_ALL_ARGS_SET) {
		
		setOffset = strtol(argv[P_SET_OFFSET], NULL, 0);
		if ((setOffset == 0) && (argv[P_SET_OFFSET][0] != '0')) {
			fprintf(stderr,"invalid set_offset: %s \n", argv[P_SET_OFFSET]);
			exit(1);
		}
		
		setValue = strtol(argv[P_SET_VALUE], NULL, 0);
		if ((setValue == 0) && (argv[P_SET_VALUE][0] != '0')) {
			fprintf(stderr,"invalid set_value: %s \n", argv[P_SET_VALUE]);
			exit(1);
		}
		
	} // if write

	uint32_t addressInt = strtol(argv[P_ADDRESS], NULL, 0);
	if (addressInt == 0) {
		about();
		fprintf(stderr,"invalid address: %s \n", argv[P_ADDRESS]);
		exit(1);
	}

	dumpOffset = strtol(argv[P_DUMP_OFFSET], NULL, 0);
	if ((dumpOffset == 0) && (argv[P_DUMP_OFFSET][0] != '0')) {
		about();
		fprintf(stderr,"invalid dump_offset: %s \n", argv[P_DUMP_OFFSET]);
		exit(1);
	}
	
	int fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (fd == -1) {
		about();
		fprintf(stderr,"can't open mem file: ");
		perror("");
		exit(1);
	}
	
	void* addressPtr = (void*)addressInt;
	
	memory = (uint32_t*)mmap(
		addressPtr, 
		size, 
		PROT_READ | PROT_WRITE,
		MAP_SHARED,
		fd,
		0
	);
	
	if (memory == MAP_FAILED) {
		about();
		fprintf(stderr,"can't map memory - ");
		perror("");
		exit(2);
	}
	
} // procArgs()


void writeValue() {
	
	if (setOffset == -1) return;
	
	memory[setOffset] = setValue;

} // writeValue()


void readPci() {

	if (setOffset != -1) {
		printf(
			"mem[ %d ($%X) ] := %d (%02X%02X:%02X%02X) \n"
			,setOffset
			,setOffset
			,setValue
			,(setValue >> 24) & 0xff
			,(setValue >> 16) & 0xff
			,(setValue >> 8) & 0xff
			,(setValue >> 0) & 0xff			
		);
	}
	
	for (int i = 0; i < size; i++) {

		if ((i % 8) == 0) {
			if (i > 0) printf("\n");
			printf(" %04lX: ",dumpOffset + i);
		}
		
		uint32_t b = memory[dumpOffset + i];
		printf(
			"%02X%02X:%02X%02X "
			,(b >> 24) & 0xff
			,(b >> 16) & 0xff
			,(b >> 8) & 0xff
			,(b >> 0) & 0xff
		);	
		
	}
	printf("\n");
	
} // readPci()


int main(int argc, char* argv[]) {

	procArgs(argc, argv);
	writeValue();
	readPci();

	return 0;
} // main()
